from django import template
from core.models import Comment
from core.forms import CommentForm

register = template.Library()


@register.inclusion_tag('comments.html')
def get_comment_list(post):
    return {'comments': Comment.objects.filter(post=post), "comment_form": CommentForm(), "post_id": post.id}
