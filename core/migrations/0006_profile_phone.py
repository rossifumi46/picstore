# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-26 05:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20170926_0428'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='phone',
            field=models.IntegerField(null=True),
        ),
    ]
