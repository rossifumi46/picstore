/**
 * Created by rossifumi46 on 17.09.17.
 */
$('.like').click(function () {
    var postid;
    var me = $(this)
    //var $counter = $(this).find('.counter');
    postid = $(this).attr("data-postid");
    console.log("success");
    $.get('/core/like/', {post_id: postid}, function (data) {
        //$counter.html(data);
        me.hide();
    });

});

$('.subscribe').click(function () {
    var me = $(this)
    var profileid = $(this).attr("data-profileid");
    $.get('/core/subscribe/', {profileid: profileid}, function (data) {
        me.hide();
    });
});

// $('#suggestion').keyup(function () {
//     var query;
//     query = $(this).val();
//     $.get('/core/suggest/', {suggestion: query}, function (data) {
//         $('#tag').html(data);
//     });
// });

// $('#login').submit(function (e) {
//     e.preventDefault();
//     var data = $(this).serialize();
//     $.ajax({
//         type: "POST",
//         url: "core/add_comment/",
//         data: data,
//         cache: false,
//         success: function (data) {
//             if (data == 'ok') {
//                 location.reload();
//             }
//             else {
//                 $('#error-login').html(data);
//             }
//         }
//     });
// });

//
//
$('#add_comment').submit(function (data) {
    var postid = $('#post_id').val();
    $.ajax(
    {
        'type': 'POST',
        'url': 'add_comment/',
        'data': {
            'csrfmiddlewaretoken': jQuery("[name=csrfmiddlewaretoken]").val(),
            'post_id': postid,
        },
    }
);
});