from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(User)
    subs = models.ManyToManyField("self", symmetrical=False, blank=True, null=True)
    photo = models.ImageField(upload_to='')
    phone = models.IntegerField(null=True)

    def __str__(self):
        return self.user.username

    def image_url(self):
        """
        Returns the URL of the image associated with this Object.
        If an image hasn't been uploaded yet, it returns a stock image
    
        :returns: str -- the image url
    
        """

        if self.photo and hasattr(self.photo, 'url'):
            return self.photo.url
        else:
            return 'profile.jpg'


class Post(models.Model):
    likes = models.IntegerField(default=0, blank=True)
    owner = models.ForeignKey('Profile', on_delete=models.CASCADE)
    # book =
    place = models.CharField(max_length=30)
    description = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)
    liked_post =        models.ManyToManyField(Profile, through='LikeDetail', related_name='liked_post')

    def __str__(self):
        return self.description


class LikeDetail(models.Model):
    liked_date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey('Post', on_delete=models.CASCADE)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)


class Comment(models.Model):
    text = models.TextField(max_length=50)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey('Post', on_delete=models.CASCADE)

    # def


class Tag(models.Model):
    post = models.ManyToManyField(Post)
    name = models.CharField(max_length=30)
    number = models.IntegerField()


class Image(models.Model):
    post = models.ForeignKey('Post', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='', null=True)

    class Meta:
        ordering = ('-created',)