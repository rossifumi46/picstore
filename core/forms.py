from django import forms
from django.contrib.auth.models import User

from .models import Image, Post, Profile, Comment

class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ('image',)


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('description', 'place')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields =('photo', 'phone')
    #
    # def __init__(self, user=None, *args, **kwargs):
    #     super(ProfileForm, self).__init__(*args, **kwargs)
    #     if not (user is None):
    #         self.fields['user'].queryset = User.objects.filter(pk=user.id)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)