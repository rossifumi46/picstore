from django.conf.urls import url, static
from django.conf import settings

from . import views

urlpatterns = [
    url(r'^$', views.subs, name='index'),
    url(r'^add_post/$', views.add_post, name='add_post'),
    url(r'^like/$', views.like_post, name='like_category'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^users/$', views.users, name='users'),
    url(r'^subscribe/$', views.subscibe, name='subscribe'),
    url(r'^suggest/$', views.suggest_tag, name='suggest'),
    url(r'^add_comment/$', views.add_comment, name='add_comment'),
]

