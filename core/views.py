from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from .models import Post, Profile, Image, Comment, Tag, LikeDetail
from .forms import ImageForm, PostForm, ProfileForm, CommentForm


@login_required
def subs(request):
    template = 'subs.html'

    profile = Profile.objects.get(user=request.user)

    subs = Profile.objects.filter(subs=profile)

    posts = Post.objects.filter(owner__in=subs).order_by('-created')

    images = Image.objects.filter(post__in=posts)

    post_liked = profile.liked_post.all()

    list = []

    liked = False

    for image in images:
        if image.post in post_liked:
            liked = True
        t = {'image': image, 'liked': liked}
        list.append(t)
        liked = False

    comment_form = CommentForm()

    dict = {'list': list, 'comment_form': comment_form}
    response = render(request, template, context=dict)

    return response


@login_required
def like_post(request):
    post_id = None
    likes = 0
    print("loh  ")
    if request.method == 'GET':
        post_id = request.GET.get('post_id')
        print(post_id)
    if post_id:

        post = Post.objects.get(pk=int(post_id))
        post.likes += 1
        profile = Profile.objects.get(user=request.user)
        # post.liked.add(profile)
        LikeDetail.objects.create(post=post, profile=profile)
        likes = post.likes
        post.save()

    return HttpResponse(likes)


@login_required
def add_post(request):

    if request.method == 'GET':
        image_form = ImageForm()
        post_form = PostForm()
        return render(request,
                      'add_post.html',
                      {'image_form': image_form,
                       'post_form': post_form})

    if request.method == 'POST':
        image_form = ImageForm(files=request.FILES)
        post_form = PostForm(data=request.POST)
        query = request.POST.get['suggestion']
        if image_form.is_valid() and post_form.is_valid():
            post = post_form.save(commit=False)
            profile = Profile.objects.get(user=request.user)
            post.owner = profile
            tag = Tag.objects.get_or_create(name=query)
            post.tag_set.add(tag)
            post.save()
            tag.number +=1
            tag.save()
            image = image_form.save(commit=False)

            image.post = post
            image.image = request.FILES['image']
            image.save()

            return redirect(reverse('index'))
        else:
            print(image_form.errors, post_form.errors)


def profile(request):
    if request.method == 'GET':
        profile_form = ProfileForm()
        return render(request, 'profile.html', {'profile_form': profile_form})

    if request.method == 'POST':
        profile = Profile.objects.create(user=request.user)
        profile_form = ProfileForm(data=request.POST, files=request.FILES, instance=profile)
        if profile_form.is_valid():
            profile_form.save()
            return redirect(reverse('index'))
        else:
            print(profile_form.errors)


@login_required
def users(request):
    if request.method == 'GET':
        profiles = Profile.objects.exclude(user=request.user)
        dict = {'profiles': profiles}
        return render(request, 'users.html', dict)


@login_required
def subscibe(request):
    print(request.GET)
    profile_id = request.GET.get('profileid')
    profile = Profile.objects.get(pk=int(profile_id))
    prof = Profile.objects.get(user=request.user)
    profile.subs.add(prof)
    return HttpResponse()


def suggest_tag(request):
    query = ''
    if request.method == 'GET':
        query = request.GET['suggestion']
        print("$", query)
        # list = query.split('#')
        # i=1;
        # for word in list:
        #     print(i, word)
        #     i+=1
        tag = Tag.objects.get(name=query)
    return render(request, 'tag.html', {'tag': tag})


def add_comment(request):
    if request.method == 'POST':
        profile = Profile.objects.get(user=request.user)
        post_id = request.POST.get('post_id')
        print(request.POST)
        print('hello')
        post = Post.objects.get(pk=int(post_id))
        print('hello', post_id)
        comment = Comment.objects.create(author=profile, post_id=int(post_id))
        comment_form = CommentForm(request.POST, instance=comment)
        if comment_form.is_valid():
            comment_form.save()
            return render(request, 'tag.html', {'comments': post.comment_set.all()})